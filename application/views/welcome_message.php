<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <?php $this->load->view('include/head.php'); ?>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
          <h1 class="display-4">Selamat Datang!</h1>
          <p class="lead">Ini merupakan master restfull api codeigniter.</p>
          <hr class="my-4">
          <p>created by Murihat.</p>
          <a class="btn btn-primary btn-lg" href="https://murihat.com" role="button">Visit my website</a>
        </div>
    </div>
    
    <?php $this->load->view('include/script.php'); ?>
</body>
</html>